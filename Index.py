from flask import Flask, render_template, jsonify, redirect, url_for, request,send_from_directory
from flask_sqlalchemy import SQLAlchemy
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
knn = KNeighborsClassifier(n_neighbors=5)
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from datetime import datetime
from sklearn.feature_extraction import text
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:@localhost:5000/TA_Mahasiswa'
db = SQLAlchemy(app)

class Mahasiswa(db.Model):
    __tablename__='Mahasiswa'
    nrp = db.Column(db.String, primary_key = True)
    name = db.Column(db.String)

    def __repr__(self):
        return self.nrp+" "+self.name

class Matakuliah(db.Model):
    __tablename__='matakuliah'
    __table_args__ = {'extend_existing': True}
    kode = db.Column(db.String, primary_key = True)
    nama_matakuliah = db.Column(db.String)

    def __repr__(self):
        return self.kode+" "+self.nama_matakuliah

class transkrip(db.Model):
    __tablename__='transkrip'
    nrp = db.Column(db.String, primary_key = True)
    kode_matakuliah = db.Column(db.String, primary_key=True)
    nilai = db.Column(db.String)

    def __repr__(self):
        return self.nrp+" "+self.kode_matakuliah+" "+self.nilai

class showTable():

    def __init__(self, kode_mk, nama_mk, nilai):
       
        self.kode_mk = kode_mk
        self.nama_mk = nama_mk
        self.nilai = nilai

class showPrediction():

    def __init__(self, kode_mk, nama_mk, nilai):
       
        self.kode_mk = kode_mk
        self.nama_mk = nama_mk
        self.nilai = nilai


@app.route("/")
def index():
    listMahasiswa = Mahasiswa.query.all()
    listMatakuliah = Matakuliah.query.all()
    taken=[]
    notTaken=[]
    nrp=listMahasiswa[0].nrp
   
    trans=['A','B+','B','C+','C','D','E']

    fileLoad = "static/mahasiswa/content_ps.csv"
    transkrip=pd.read_csv(fileLoad,sep=';')

    transkrip = transkrip.loc[transkrip['NRP']==int(nrp)]

    taken = transkrip['KodeMK']
    nilai = transkrip['Nilai']


    isTaken=False
    mk_taken = []
    k=0
    for i in listMatakuliah:
        for j in taken:
            if(i.kode == j):
                isTaken = True
                mk_taken.append(showTable(j,i.nama_matakuliah,trans[nilai[k]-1]))
                k+=1
        
        if(isTaken==False):
            notTaken.append(i)
        isTaken = False
    
    return render_template("Select_Mahasiswa.html", data = mk_taken, Mahasiswa=listMahasiswa[0], notTaken = notTaken, listMahasiswa=listMahasiswa)

@app.route("/showMahasiswa",methods=['POST'])
def showMahasiswa():
    nrp = request.form['nrp']
    listMahasiswa = Mahasiswa.query.all()
    listMatakuliah = Matakuliah.query.all()
    taken=[]
    notTaken=[]
    
    mahasiswa = Mahasiswa.query.filter(Mahasiswa.nrp == nrp).first()

    trans=['A','B+','B','C+','C','D','E']

    fileLoad = "static/mahasiswa/content_ps.csv"
    transkrip=pd.read_csv(fileLoad,sep=';')

    transkrip = transkrip.loc[transkrip['NRP']==int(nrp)]
    transkrip = transkrip.reset_index(drop=True)

    taken = transkrip['KodeMK']
    nilai = transkrip['Nilai']
    print(nilai)

    isTaken=False
    mk_taken = []
    k=0
    for i in listMatakuliah:
        for j in taken:
            if(i.kode == j):
                isTaken = True
                mk_taken.append(showTable(j,i.nama_matakuliah,trans[nilai[k]-1]))
                k+=1
        
        if(isTaken==False):
            notTaken.append(i)
        isTaken = False

                    
    
    return render_template("Select_Mahasiswa.html", listMahasiswa=listMahasiswa, data = mk_taken, Mahasiswa=mahasiswa, notTaken = notTaken)


@app.route('/uploadUI')
def uploadUI():
    listMatakuliah = Matakuliah.query.all()
    return render_template("uploadUI.html", listMK = listMatakuliah)

def allowed_file(filename):
    ALLOWED_EXTENSIONS = ['pdf']
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['POST'])
def upload():
   app.config['UPLOAD_FOLDER'] ='upl'
   if request.method == 'POST':
       file=request.files['file']
       matkul = request.form['matkul']
       if file and allowed_file(file.filename):
           now = datetime.now()
           name = now.strftime("%Y-%m-%d-%H-%M-%S-%f")
           filename = os.path.join(app.config['UPLOAD_FOLDER'], "%s.%s" % (name, file.filename.rsplit('.', 1)[1]))
           file.save(filename)
           path = "static/matakuliah/materi/"+matkul+"/txt/"+name+".txt"
           os.system("python static/pdf2txt.py -o "+path+" "+filename)
           return redirect(url_for('uploadUI'))  
       else:
           return redirect(url_for('index')) 

@app.route("/rekomendasi",methods=['POST'])
def rekomendasi():
    factory = StopWordRemoverFactory()
    stopwords = factory.get_stop_words()
    cv1 = TfidfVectorizer(min_df=1, stop_words=stopwords)

    nrp = request.form['nrp']

    arrRekomendasi = request.form.getlist('prediksi')


    fileLoad = "static/mahasiswa/content_ps.csv"
    transkrip=pd.read_csv(fileLoad,sep=';')
    transkrip = transkrip.loc[transkrip['NRP']==int(nrp)]
    transkrip.loc[transkrip['Nilai']==1,'Nilai']=1
    transkrip.loc[transkrip['Nilai']==2,'Nilai']=1
    transkrip.loc[transkrip['Nilai']==3,'Nilai']=1
    transkrip.loc[transkrip['Nilai']==4,'Nilai']=2
    transkrip.loc[transkrip['Nilai']==5,'Nilai']=2
    transkrip.loc[transkrip['Nilai']==6,'Nilai']=3
    transkrip.loc[transkrip['Nilai']==7,'Nilai']=3

    listMK = transkrip['KodeMK']
    train = []
    for i in listMK:
        pathSilabus = 'static/matakuliah/silabus/'+i+'.txt'
        silabus=""
        if(os.path.isfile(pathSilabus)):
            with open(pathSilabus,  encoding='utf-8', errors='ignore') as f:
                contents = f.read()
            contents = contents.strip()
            silabus = contents
        pathMateri = 'static/matakuliah/materi/'+i+'/txt/'
        materi = ""
        if(os.path.isdir(pathMateri)):
            for filename in os.listdir(pathMateri):
                path = pathMateri+filename
                with open(path,  encoding='utf-8', errors='ignore') as f:
                    contents = f.read()
                contents = contents.strip()
                materi = materi+" "+contents
        train.append(silabus+" "+materi)

    df_text = np.asarray(train)
    df_nilai = transkrip['Nilai']


    model = MultinomialNB()

    X = cv1.fit_transform(df_text)
    chi2_selector = SelectKBest(chi2, k=10)
    chi2score = chi2_selector.fit_transform(X, df_nilai)
    
    X= chi2_selector.transform(X)
    df_nilai = df_nilai.astype(int)
    model.fit(X, df_nilai)

    texts = []
    for i in arrRekomendasi:
        pathSilabus = 'static/matakuliah/silabus/'+i+'.txt'
        silabus=""
        if(os.path.isfile(pathSilabus)):
            with open(pathSilabus,  encoding='utf-8', errors='ignore') as f:
                contents = f.read()
            contents = contents.strip()
            silabus = contents
        pathMateri = 'static/matakuliah/materi/'+i+'/txt/'
        materi = ""
        if(os.path.isdir(pathMateri)):
            for filename in os.listdir(pathMateri):
                path = pathMateri+filename
                with open(path,  encoding='utf-8', errors='ignore') as f:
                    contents = f.read()
                contents = contents.strip()
                materi = materi+" "+contents
        texts.append(silabus+" "+materi)
    texts=np.asarray(texts)
    texts = cv1.transform(texts)
    texts = chi2_selector.transform(texts)
    predict = model.predict(texts)
    predict = predict.tolist()
    predict.sort()
    
    #-------------------------------------------
    model = KNeighborsClassifier(n_neighbors=3, weights='distance')
    df=pd.read_csv("static/mahasiswa/transkrip_ps.csv",sep=';')

    mhs = df.loc[df['NRP']==int(nrp),]
    df = df.loc[df['NRP']!=int(nrp),]
    
    df = df.drop(['NRP'],axis=1)
    knnTest = df[arrRekomendasi]
    knnTrain = df.drop(arrRekomendasi, axis=1)
    model.fit(knnTrain, knnTest.values)
    

    mhs = mhs.drop(arrRekomendasi, axis=1)
    mhs = mhs.drop(['NRP'], axis=1)
    nilai =['Lulus dengan Baik','Lulus dengan Baik','Lulus dengan Baik','Lulus','Lulus','Tidak Lulus','Tidak Lulus']
    knnPredict = model.predict(mhs)[0]
    
    listKNN = []
    temp =0
    for i in arrRekomendasi:
        MKKNN = Matakuliah.query.filter(Matakuliah.kode == i).first()
        if(len(arrRekomendasi)>1):
            knnPredict.sort()
            item = showPrediction(MKKNN.kode, MKKNN.nama_matakuliah, nilai[knnPredict[temp]-1])
        else:
            item = showPrediction(MKKNN.kode, MKKNN.nama_matakuliah, nilai[knnPredict-1])
        temp+=1
        listKNN.append(item)
        
    



    temp=0
    listshow = []
    listMK = Matakuliah.query.all()
    for i in arrRekomendasi:
        for  j in listMK:
            if(j.kode == i):
                item = showPrediction(j.kode, j.nama_matakuliah, nilai[predict[temp]-1])
                temp+=1
                listshow.append(item)
                
    mahasiswa = Mahasiswa.query.filter(Mahasiswa.nrp == nrp).first()


    return render_template("show_prediction.html", mahasiswa=mahasiswa, show= listshow, content = predict, collabo = listKNN)


if __name__ == "__main__":
    app.run()